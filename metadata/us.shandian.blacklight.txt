AntiFeatures:NonFreeNet
Categories:Internet
License:GPLv3
Web Site:https://github.com/PaperAirplane-Dev-Team/BlackLight/blob/HEAD/README.md
Source Code:https://github.com/PaperAirplane-Dev-Team/BlackLight
Issue Tracker:https://github.com/PaperAirplane-Dev-Team/BlackLight/issues

Auto Name:BlackLight
Summary:Sina Weibo client
Description:
Client for Weibo, the popular chinese social network.
.

Repo Type:git
Repo:https://github.com/PaperAirplane-Dev-Team/BlackLight

Build:1.0.6.1,18
    commit=1.0.6.1
    target=android-19
    extlibs=android/android-support-v4.jar
    disable=jars,mismatches,r

Auto Update Mode:None
Update Check Mode:Tags
Current Version:1.0.6.1
Current Version Code:18
